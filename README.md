CONTENTS OF THIS FILE
=====================
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------
The Bootstrap Components module integrate various
[bootstrap](http://getbootstrap.com) component into drupal.


REQUIREMENTS
------------


INSTALLATION
------------


CONFIGURATION
-------------


TROUBLESHOOTING
---------------


FAQ
---


MAINTAINERS
-----------
Current maintainers:
 * Luhur Abdi Rizal ([el7cosmos](https://drupal.org/u/el7cosmos))
